import { Directive, ElementRef, HostListener, Input } from "@angular/core";
@Directive({
  selector: "[myNumberOnly]",
})
export class NumberOnlyDirective {
  @Input() max: Number;
  @Input() min: Number;
  @Input() msg: String;
  oldValue;
  constructor(private element: ElementRef) {
    if (this.min == undefined) this.min = 0;
    if (this.max == undefined) this.max = 1000;
    if (this.msg == undefined) this.msg = "No puede Digitar un Numero";
  }

  @HostListener("keydown", ["$event"]) keyDown(event) {
    let e = <KeyboardEvent>event;
    if (
      e.keyCode === 187 ||
      e.keyCode === 189 ||
      e.keyCode === 69 ||
      e.keyCode === 188
    ) {
      alert("No puede ingresar ese simbolo");
      return false;
    }
    return true;
  }

  @HostListener("keyup", ["$event"]) keyUp(event) {
    this.oldValue = (this.element.nativeElement as HTMLInputElement).value;
    if (parseFloat(this.oldValue) > this.max) {
      (this.element.nativeElement as HTMLInputElement).value = String(
        this.oldValue
      ).slice(0, -1);
      alert(this.msg + " mayores a " + this.min + " o menores a " + this.max);
    }
    return true;
  }

  @HostListener("change", ["$event"]) change(event) {
    this.oldValue = (this.element.nativeElement as HTMLInputElement).value;
    if (
      parseFloat(this.oldValue) < this.min ||
      parseFloat(this.oldValue) > this.max
    ) {
      (this.element.nativeElement as HTMLInputElement).value = "";
      alert(this.msg + " mayores a " + this.min + " o menores a  " + this.max);
      //(this.element.nativeElement as HTMLInputElement).focus();
    }
    return true;
  }
}
