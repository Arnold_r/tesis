import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { GeneralService } from "../providers/general.service";

@Component({
  selector: "app-material",
  templateUrl: "./material.page.html",
  styleUrls: ["./material.page.scss"],
})
export class MaterialPage implements OnInit {
  titulo = "agregar un nuevo material";

  formMaterial: FormGroup;
  Absorvente = false;
  public materials;

  constructor(private fb: FormBuilder, private service: GeneralService) {}

  ngOnInit() {
    this.formMaterial = this.fb.group({
      nombre: [, Validators.required],
      alias: [, Validators.required],
      mYoung: [, Validators.required],
      densidad: [
        ,
        Validators.compose([
          Validators.required,
          Validators.min(0),
          Validators.max(21500),
        ]),
      ],
      coefInterno: [, Validators.required],
      factorN: [, Validators.required],
      cPoison: [, Validators.required],
      Absorvente: [{ value: false }, Validators.required],
    });
    this.service.getMaterials().then((_materials) => {
      this.materials = _materials;
     
    });
  }

  delete(i) {
    this.service.deleteMaterial(i).then((_materials) => {
      this.materials = _materials;
    });
  }

  save(formulario: FormGroup) {

    if (formulario.valid) {
      this.service.addMaterial(formulario.value).then((_materiales) => {
        formulario.reset();
      });
      alert("Guardado");
      this.service.getMaterials().then((_materials) => {
        this.materials = _materials;
      });
    } else {
      alert("LLENA LOS DATOS BIEN");
    }
  }
}
