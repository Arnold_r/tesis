import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "absorvente",
})
export class TypePipe implements PipeTransform {
  transform(
    value: Array<any>,
    absorvente: boolean,
    barron: boolean
  ): Array<any> {
    if (!value) return value;
    if (barron && absorvente)
      return value.filter(
        (item) => item.Absorvente == absorvente && item.alias == "AIRE"
      );
    return value.filter((item) => item.Absorvente == absorvente);
  }
}
