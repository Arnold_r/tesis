import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class GeneralService {
  constructor(private http: HttpClient) { }

  getLocalMaterials() {
    return this.http.get('assets/materials/materials.json');
  }
  setMaterials(materiales) {
    localStorage.setItem('materiales', JSON.stringify(materiales));
    return materiales.sort(this.sortMaterials);
  }

  async getMaterials() {
    let materiales = JSON.parse(localStorage.getItem('materiales'));

    if (materiales) {
      return materiales.sort(this.sortMaterials);
    }
    return [];
  }

  async addMaterial(material) {
    let materiales: Array<any> =
      JSON.parse(localStorage.getItem('materiales')) || [];
    materiales.push(material);
    localStorage.setItem('materiales', JSON.stringify(materiales));
    return materiales.sort(this.sortMaterials);
  }
  async deleteMaterial(i) {
    let materiales: Array<any> =
      JSON.parse(localStorage.getItem('materiales')) || [];
    materiales.sort(this.sortMaterials);
    materiales.splice(i, 1);
    localStorage.setItem('materiales', JSON.stringify(materiales));
    return materiales;
  }

  sortMaterials(a, b) {
    // Use toUpperCase() to ignore character casing
    const nom1 = a.alias.toUpperCase();
    const nom2 = b.alias.toUpperCase();

    let comparison = 0;
    if (nom1 > nom2) {
      comparison = 1;
    } else if (nom1 < nom2) {
      comparison = -1;
    }
    return comparison;
  }
}
