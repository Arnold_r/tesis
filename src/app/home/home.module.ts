import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { showDatosComponent } from './../components/showDatos/showDatos.component';
import { ModalCondiciones } from './../components/modals/modalCondiciones.component';
import { TypePipe } from '../pipes/type.pipe';
import { NumberOnlyDirective } from '../@shared/number-only-directive';
import { SharedModule } from '../@shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    SharedModule,
    HomePageRoutingModule,
  ],
  declarations: [
    HomePage,
    showDatosComponent,
    ModalCondiciones,
    TypePipe,
  ],
})
export class HomePageModule {}
