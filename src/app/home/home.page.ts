import {
  Component,
  ElementRef,
  ViewChild,
  Input,
  OnInit,
  DebugElement,
} from "@angular/core";
import {
  Validators,
  FormGroup,
  FormBuilder,
  FormArray,
  FormControl,
} from "@angular/forms";
import { Router } from "@angular/router";
import { GeneralService } from "../providers/general.service";
import { Chart } from "chart.js";
import { ModalController } from "@ionic/angular";
import { ModalCondiciones } from "./../components/modals/modalCondiciones.component";

//import { clear } from 'console';

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage implements OnInit {
  @ViewChild("lineCanvasProD") lineCanvasProD;
  @ViewChild("lineCanvasProDNT") lineCanvasProDNT;
  @ViewChild("lineCanvasD") lineCanvasD;
  @ViewChild("lineCanvasDnT") lineCanvasDnT;
  @ViewChild("lineCanvasLR") lineCanvasLR;
  @ViewChild("lineCanvasFB") lineCanvasFB;
  @ViewChild("lineCanvasUD") lineCanvasUD;
  @ViewChild("lineCanvasLRstc") lineCanvasLRstc;
  @ViewChild("lineCanvasFBstc") lineCanvasFBstc;
  @ViewChild("lineCanvasUDstc") lineCanvasUDstc;
  @ViewChild("lineCanvasNc") lineCanvasNc;
  @ViewChild("lineCanvas") lineCanvas;
  @ViewChild("lineCanvasBarras") lineCanvasBarras;
  @ViewChild("lineCanvasBarrasPromedio") lineCanvasBarrasPromedio;

  lineChartProD: any;
  lineChartProDNT: any;
  lineChartD: any;
  lineChartDnt: any;
  lineCanvasPromedio: any;
  lineChartNc: any;
  lineChartRL: any;
  lineChartFB: any;
  lineChartUP: any;
  lineChartRLstc: any;
  lineChartFBstc: any;
  lineChartUPstc: any;
  lineChart: any;
  barrasChart: any;
  datosBarras: any[] = [];
  densidad = 1.24;
  propagacion = 343;
  metodo = "sharp";
  seleccionado;
  fuenteSonora;
  r1W;
  espectroCalculo;
  espectroCtrCalculo;
  resultadoStc;
  graficoStc = [];
  difNiveles = [];
  difnivelesDnt = [];

  splFrecuencias: number[] = [];

  dNt = 0;
  tr: FormGroup;
  sp: FormArray = new FormArray([]);

  f = [
    63,
    80,
    100,
    125,
    160,
    200,
    250,
    315,
    400,
    500,
    630,
    800,
    1000,
    1250,
    1600,
    2000,
    2500,
    3150,
    4000,
    5000,
    6300,
    8000,
  ];

  f2 = [
    63,
    80,
    100,
    125,
    160,
    200,
    250,
    315,
    400,
    500,
    630,
    800,
    1000,
    1250,
    1600,
    2000,
    2500,
    3150,
    4000,
    5000,
    6300,
    8000,
    "Total Lin",
  ];

  tl = [];

  refVal = [, , 33, 36, 39, 42, 45, 48, 51, 52, 53, 54, 55, 56, 56, 56, 56, 56];

  espectroC = [
    ,
    ,
    -29,
    -26,
    -23,
    -21,
    -19,
    -17,
    -15,
    -13,
    -12,
    -11,
    -10,
    -9,
    -9,
    -9,
    -9,
    -9,
  ];

  espectroCtr = [
    ,
    ,
    -20,
    -20,
    -18,
    -16,
    -15,
    -14,
    -13,
    -12,
    -11,
    -9,
    -8,
    -9,
    -10,
    -11,
    -13,
    -15,
  ];

  stc = [, , , 34, 37, 40, 43, 46, 49, 50, 51, 52, 53, 54, 54, 54, 54, 54, 54];

  nC15 = [
    42.59,
    38.88,
    35.4,
    32.05,
    28.68,
    26.03,
    23.78,
    21.78,
    19.9,
    18.18,
    16.36,
    14.46,
    12.74,
    11.16,
    9.71,
    8.72,
    8.06,
    7.67,
    7.49,
    7.43,
    7.41,
    7.41,
  ];
  nC20 = [
    46.34,
    42.96,
    39.65,
    36.32,
    32.81,
    29.95,
    27.49,
    25.38,
    23.59,
    22.14,
    20.7,
    19.2,
    17.79,
    16.42,
    15.03,
    13.98,
    13.18,
    12.62,
    12.29,
    12.15,
    12.1,
    12.08,
  ];
  nC25 = [
    49.32,
    46.2,
    43.13,
    40.03,
    36.75,
    34.06,
    31.74,
    29.76,
    28.09,
    26.75,
    25.44,
    24.07,
    22.76,
    21.45,
    20.1,
    19.03,
    18.18,
    17.55,
    17.15,
    16.97,
    16.89,
    16.87,
  ];
  nC30 = [
    52.31,
    49.44,
    46.62,
    43.77,
    40.72,
    38.19,
    35.96,
    33.98,
    32.23,
    30.77,
    29.34,
    27.92,
    26.67,
    25.55,
    24.55,
    23.9,
    23.48,
    23.24,
    23.14,
    23.11,
    23.1,
    23.1,
  ];
  nC35 = [
    55.38,
    52.81,
    50.24,
    47.61,
    44.76,
    42.35,
    40.21,
    38.32,
    36.68,
    35.36,
    34.1,
    32.85,
    31.72,
    30.68,
    29.68,
    28.98,
    28.49,
    28.18,
    28.03,
    27.97,
    27.95,
    27.95,
  ];
  nC40 = [
    59.4,
    57.2,
    54.95,
    52.56,
    49.87,
    47.5,
    45.3,
    43.29,
    41.53,
    40.15,
    38.9,
    37.73,
    36.68,
    35.71,
    34.76,
    34.06,
    33.56,
    33.22,
    33.04,
    32.97,
    32.94,
    32.94,
  ];
  nC45 = [
    62.14,
    60.09,
    57.98,
    55.75,
    53.22,
    51.01,
    48.96,
    47.12,
    45.56,
    44.39,
    43.39,
    42.46,
    41.64,
    40.82,
    39.96,
    39.25,
    38.66,
    38.2,
    37.89,
    37.73,
    37.66,
    37.63,
  ];
  nC50 = [
    66.31,
    64.07,
    61.84,
    59.55,
    57.06,
    54.98,
    53.16,
    51.59,
    50.29,
    49.29,
    48.38,
    47.46,
    46.61,
    45.76,
    44.88,
    44.19,
    43.64,
    43.23,
    42.98,
    42.86,
    42.81,
    42.79,
  ];
  nC55 = [
    69.2,
    67.06,
    64.94,
    62.77,
    60.43,
    58.51,
    56.84,
    55.45,
    54.34,
    53.54,
    52.83,
    52.12,
    51.45,
    50.75,
    49.98,
    49.33,
    48.76,
    48.29,
    47.95,
    47.76,
    47.66,
    47.62,
  ];
  nC60 = [
    72.1,
    70.37,
    68.62,
    66.82,
    64.85,
    63.18,
    61.71,
    60.43,
    59.37,
    58.57,
    57.86,
    57.14,
    56.46,
    55.75,
    54.98,
    54.32,
    53.75,
    53.28,
    52.93,
    52.74,
    52.64,
    52.6,
  ];
  nC65 = [
    75.1,
    73.56,
    72.01,
    70.43,
    68.7,
    67.26,
    66.0,
    64.92,
    64.03,
    63.36,
    62.74,
    62.11,
    61.49,
    60.83,
    60.09,
    59.43,
    58.83,
    58.31,
    57.91,
    57.65,
    57.51,
    57.44,
  ];
  nC70 = [
    78.22,
    77.01,
    75.75,
    74.4,
    72.84,
    71.45,
    70.15,
    68.98,
    68.02,
    67.36,
    66.88,
    66.54,
    66.28,
    66.02,
    65.71,
    65.4,
    65.05,
    64.68,
    64.28,
    63.93,
    63.61,
    63.33,
  ];

  absor;
  sabine;
  and: number;
  mon: number;

  lista = ["SI", "NO"];

  @ViewChild("upload", { static: false }) upload: ElementRef;

  view = { particion: "Unaparticion", proyecto: "Proyecto" };

  types = { simple: "Simple", doble: "Doble", compuesta: "Compuesta" };

  steps = { normal: "Normal", spl: "Spl", resultados: "Resultados" };

  datosPresion = { tr: "tr", sp: "sp" };

  caras = {
    derecha: "Derecha",
    izquierda: "Izquierda",
    frontal: "Frontal",
    trasera: "Trasera",
    superior: "Superior",
    inferior: "Inferior",
  };

  paramCaras = {
    Derecha: {
      tl: [],
      sp: [],
      fachada: "",
      fuenteSonora: "",
      absor: 0,
      guardado: false,
      resultadoStc: 0,
      rwRes: [],
      c: 0,
      ctr: 0,
      r1W: 0,
      stc: [],
      arrayStc: [],
      espectrosD: [],
      dnt1: [],
      dNt: 0,
      spIn: [],
      difNiveles: [],
    },
    Izquierda: {
      tl: [],
      sp: [],
      fachada: "",
      fuenteSonora: "",
      absor: 0,
      guardado: false,
      resultadoStc: 0,
      rwRes: [],
      c: 0,
      ctr: 0,
      r1W: 0,
      stc: [],
      arrayStc: [],
      espectrosD: [],
      dnt1: [],
      dNt: 0,
      spIn: [],
      difNiveles: [],
    },
    Frontal: {
      tl: [],
      sp: [],
      fachada: "",
      fuenteSonora: "",
      absor: 0,
      guardado: false,
      resultadoStc: 0,
      rwRes: [],
      c: 0,
      ctr: 0,
      r1W: 0,
      stc: [],
      arrayStc: [],
      espectrosD: [],
      dnt1: [],
      dNt: 0,
      spIn: [],
      difNiveles: [],
    },
    Trasera: {
      tl: [],
      sp: [],
      fachada: "",
      fuenteSonora: "",
      absor: 0,
      guardado: false,
      resultadoStc: 0,
      rwRes: [],
      c: 0,
      ctr: 0,
      r1W: 0,
      stc: [],
      arrayStc: [],
      espectrosD: [],
      dnt1: [],
      dNt: 0,
      spIn: [],
      difNiveles: [],
    },
    Superior: {
      tl: [],
      sp: [],
      fachada: "",
      fuenteSonora: "",
      absor: 0,
      guardado: false,
      resultadoStc: 0,
      rwRes: [],
      c: 0,
      ctr: 0,
      r1W: 0,
      stc: [],
      arrayStc: [],
      espectrosD: [],
      dnt1: [],
      dNt: 0,
      spIn: [],
      difNiveles: [],
    },
    Inferior: {
      tl: [],
      sp: [],
      fachada: "",
      fuenteSonora: "",
      absor: 0,
      guardado: false,
      resultadoStc: 0,
      rwRes: [],
      c: 0,
      ctr: 0,
      r1W: 0,
      stc: [],
      arrayStc: [],
      espectrosD: [],
      dnt1: [],
      dNt: 0,
      spIn: [],
      difNiveles: [],
    },
  };

  public materials;

  currentView;

  currentType;

  currentStep;

  currentCara;

  currentConfiguration;

  currentDatos;

  ruta = "/assets/img/plano3d.png";

  rutaType = "/assets/img/simple-2.png";

  formSimple: FormGroup;
  formDoble: FormGroup;

  formCompuesta: FormArray;
  derecha;
  izquierda;
  frontal;
  trasera;
  superior;
  inferior;

  constructor(
    public modalController: ModalController,
    private fb: FormBuilder,
    private router: Router,
    public service: GeneralService
  ) {}

  ngOnInit() {
    this.currentStep = this.steps.normal;
    this.service.getMaterials().then((_materials) => {
      if (_materials.length) {
        this.materials = _materials;
      } else {
        this.service.getLocalMaterials().subscribe((_materials) => {
          this.materials = _materials;
          this.service.setMaterials(_materials);
        });
      }
    });
    this.formSimple = this.createSimple();
    this.formDoble = this.createDoble();

    this.formCompuesta = this.fb.array([]);

    this.tr = this.initTr();

    this.initDataSpl();
  }

  initDataSpl() {
    for (let i = 0; i < this.f.length; i++) {
      (this.tr.get("_tr") as FormArray).push(
        new FormControl(null, Validators.required)
      );
      (this.tr.get("_rf") as FormArray).push(
        new FormControl(null, Validators.required)
      );
      this.sp.push(
        new FormControl(
          null,
          Validators.compose([
            Validators.required,
            Validators.min(0),
            Validators.max(140),
          ])
        )
      );
      this.splFrecuencias.push(null);
    }
  }

  cleanData() {
    this.izquierda = null;
    this.derecha = null;
    this.frontal = null;
    this.superior = null;
    this.trasera = null;
    this.inferior = null;
    this.currentCara = null;
    this.currentDatos = null;
    this.currentView = null;
    this.lineChart = null;
    this.currentStep = this.steps.normal;
    for (let i = 0; i < this.f.length; i++) {
      this.sp.controls[i].setValue(null);
      this.formTr.controls[i].setValue(null);
      this.tl[i] = null;
    }
    this.tr.controls.volumen.setValue("");
    this.formSimple = this.createSimple();
    this.formDoble = this.createDoble();
    this.formCompuesta = this.fb.array([]);
  }

  get formTr(): FormArray {
    return this.tr.get("_tr") as FormArray;
  }

  get formRf(): FormArray {
    return this.tr.get("_rf") as FormArray;
  }

  async presentModalCondiciones() {
    const modal = await this.modalController.create({
      component: ModalCondiciones,
      componentProps: {
        densidad: this.densidad,
        propagacion: this.propagacion,
        metodo: this.metodo,
      },
    });

    modal.onDidDismiss().then((dataReturned: any) => {
      if (dataReturned.data !== null) {
        this.densidad = dataReturned.data.densidad;
        this.propagacion = dataReturned.data.propagacion;
        this.metodo = dataReturned.data.metodo;

        this.cleanData();
      }
    });
    return await modal.present();
  }

  Reset() {
    this.izquierda = null;
    this.derecha = null;
    this.frontal = null;
    this.superior = null;
    this.trasera = null;
    this.inferior = null;
    this.currentCara = null;
    this.datosBarras = [];
    if (this.lineChart) {
      this.lineChart.clear();
      this.lineChart.destroy();
      this.lineChart = null;
    }
    if (this.lineChartD) {
      this.lineChartD.clear();
      this.lineChartD.destroy();
      this.lineChartD = null;
    }
    if (this.lineChartDnt) {
      this.lineChartDnt.clear();
      this.lineChartDnt.destroy();
      this.lineChartDnt = null;
    }
    if (this.lineChartNc) {
      this.lineChartNc.clear();
      this.lineChartNc.destroy();
      this.lineChartNc = null;
    }
    if (this.lineChartRL) {
      this.lineChartRL.clear();
      this.lineChartRL.destroy();
      this.lineChartRL = null;
    }
    if (this.lineChartFB) {
      this.lineChartFB.clear();
      this.lineChartFB.destroy();
      this.lineChartFB = null;
    }
    if (this.lineChartUP) {
      this.lineChartUP.clear();
      this.lineChartUP.destroy();
      this.lineChartUP = null;
    }
    if (this.lineChartRLstc) {
      this.lineChartRLstc.clear();
      this.lineChartRLstc.destroy();
      this.lineChartRLstc = null;
    }
    if (this.lineChartFBstc) {
      this.lineChartFBstc.clear();
      this.lineChartFBstc.destroy();
      this.lineChartFBstc = null;
    }
    if (this.lineChartUPstc) {
      this.lineChartUPstc.clear();
      this.lineChartUPstc.destroy();
      this.lineChartUPstc = null;
    }
    if (this.barrasChart) {
      this.barrasChart.clear();
      this.barrasChart.destroy();
      this.barrasChart = null;
    }
    if (this.lineChartProD) {
      this.lineChartProD.clear();
      this.lineChartProD.destroy();
      this.lineChartProD = null;
    }
    if (this.lineChartProDNT) {
      this.lineChartProDNT.clear();
      this.lineChartProDNT.destroy();
      this.lineChartProDNT = null;
    }
  }

  createSimple(): FormGroup {
    return this.fb.group({
      type: [this.types.simple],
      material: [, Validators.required],
      largo: [, Validators.required],
      ancho: [, Validators.required],
      alto: [, Validators.required],
      contenido: [""],
    });
  }

  createDoble(): FormGroup {
    if (this.metodo == "sharp") {
      return this.fb.group({
        type: [this.types.doble],
        material: [, Validators.required],
        material1: [, Validators.required],
        material2: [, Validators.required],
        largo: [, Validators.required],
        alto: [, Validators.required],
        espesor: [, Validators.required],
        distancia: [, Validators.required],
        espesor2: [, Validators.required],
        constanteB: [, Validators.required],
        contenido: [""],
      });
    } else {
      return this.fb.group({
        type: [this.types.doble],
        material: [, Validators.required],
        material1: [, Validators.required],
        material2: [, Validators.required],
        largo: [, Validators.required],
        alto: [, Validators.required],
        espesor: [, Validators.required],
        distancia: [, Validators.required],
        espesor2: [, Validators.required],
        contenido: [""],
      });
    }
  }

  initTr(): FormGroup {
    return this.fb.group({
      _tr: new FormArray([]),
      _rf: new FormArray([]),
      volumen: [, Validators.required],
    });
  }

  open() {
    this.upload.nativeElement.click();
  }

  enable(view) {
    this.cleanData();
    this.Reset();
    this.currentView = view;
    this.currentType = null;
    this.ruta = "/assets/img/plano3d.png";
    this.rutaType = "/assets/img/simple-2.png";
  }

  enableStep(step) {
    this.currentStep = step;
  }

  enableCara(cara, spl?) {
    this.currentCara = cara;
    this.ruta = "/assets/img/Cara" + cara + ".png";
    if (spl) {
      this.currentDatos = this.datosPresion.sp;
    }
  }

  enableType(type) {
    this.currentType = type;
    this.formCompuesta = this.fb.array([]);
    this.rutaType = "/assets/img/" + type + ".png";
  }

  addSub(type) {
    if (this.formCompuesta.controls.length < 3) {
      if (type == this.types.simple)
        this.formCompuesta.push(this.createSimple());
      if (type == this.types.doble) this.formCompuesta.push(this.createDoble());
    } else alert("Máximo 3 ítems");
  }

  calcSimple(formSimple) {
    if (formSimple.valid) {
      this.getSimple(formSimple.value).then((_res) => {
        this.tl = _res.tl;
        this.absor = formSimple.value.largo * formSimple.value.alto;

        this.plot();
      });
    } else alert("Ingrese todos los datos");
  }
  async calcDoble(formDoble) {
    if (formDoble.valid) {
      const suma =
        formDoble.value.espesor +
        formDoble.value.distancia +
        formDoble.value.espesor2;
      if (suma <= 0.32) {
        this.getDoble(formDoble.value).then((_res) => {
          this.tl = _res.tl;
          this.absor = formDoble.value.largo * formDoble.value.alto;
          this.plot();
        });
      } else {
        alert("la suma de los espesores y distancia no puede ser mayor a 0.36");
      }
    } else alert("Ingrese todos los datos");
  }
  calcCompuesta(formCompuesta) {
    if (formCompuesta.valid) {
      const data = formCompuesta.value;
      let sw = true;
      for (let i = 0; i < data.length; i++) {
        let form = data[i];
        if (form.type == this.types.doble) {
          const suma = form.espesor + form.distancia + form.espesor2;
          if (suma > 0.32) {
            sw = false;
          }
        }
      }
      if (sw) {
        this.getCompuesta(formCompuesta.value).then((_res) => {
          this.tl = _res;
          this.absor =
            formCompuesta.value[0].largo * formCompuesta.value[0].alto;
          this.plot();
        });
      } else {
        alert(
          "Alguna de las superficies dobles de su particion compuesta supera el valor de ancho total de 0.32"
        );
      }
    } else alert("Ingrese todos los datos");
  }
  async calcAll() {
    if (this.lineChart) {
      this.lineChart.clear();
      this.lineChart.destroy();

      this.lineChart = null;
    }
    this.currentType = null;

    const tlDerecha: any = await this.calcCara(
      this.derecha,
      this.caras.derecha
    );
    const tlIzquierda: any = await this.calcCara(
      this.izquierda,
      this.caras.izquierda
    );
    const tlFrontal: any = await this.calcCara(
      this.frontal,
      this.caras.frontal
    );
    const tlTrasera: any = await this.calcCara(
      this.trasera,
      this.caras.trasera
    );
    const tlSuperior: any = await this.calcCara(
      this.superior,
      this.caras.superior
    );
    let tlInferior: any = {};
    if (this.inferior)
      tlInferior = await this.calcCara(this.inferior, this.caras.inferior);
    this.paramCaras.Derecha.tl = tlDerecha.tl;
    this.paramCaras.Izquierda.tl = tlIzquierda.tl;
    this.paramCaras.Frontal.tl = tlFrontal.tl;
    this.paramCaras.Trasera.tl = tlTrasera.tl;
    this.paramCaras.Superior.tl = tlSuperior.tl;
    this.paramCaras.Inferior.tl = tlInferior.tl;

    this.plotAll(
      tlDerecha.tl,
      tlIzquierda.tl,
      tlFrontal.tl,
      tlTrasera.tl,
      tlSuperior.tl,
      tlInferior.tl
    );
  }

  async calcCara(cara, nombre) {
    if (cara.type == this.types.simple) {
      this.paramCaras[nombre].absor = cara.form.largo * cara.form.alto;
      return this.getSimple(cara.form);
    }

    if (cara.type == this.types.doble) {
      this.paramCaras[nombre].absor = cara.form.largo * cara.form.alto;

      return this.getDoble(cara.form);
    }
    if (cara.type == this.types.compuesta) {
      if (cara.form[0].type == this.types.simple) {
        this.paramCaras[nombre].absor = cara.form[0].largo * cara.form[0].alto;
      }
      if (cara.form[0].type == this.types.doble) {
        this.paramCaras[nombre].absor = cara.form[0].largo * cara.form[0].alto;
      }
      return this.getCompuesta(cara.form).then((_res) => {
        return { tl: _res };
      });
    }
  }

  save(form, cara) {
    if (form.valid) {
      const formSave = JSON.parse(JSON.stringify(form.value));
      if (this.caras.derecha == cara)
        this.derecha = { type: this.currentType, form: formSave };

      if (this.caras.izquierda == cara)
        this.izquierda = { type: this.currentType, form: formSave };

      if (this.caras.frontal == cara)
        this.frontal = { type: this.currentType, form: formSave };

      if (this.caras.trasera == cara)
        this.trasera = { type: this.currentType, form: formSave };

      if (this.caras.superior == cara)
        this.superior = { type: this.currentType, form: formSave };

      if (this.caras.inferior == cara)
        this.inferior = { type: this.currentType, form: formSave };
      form.reset();
    } else alert("Ingrese todos los datos");
  }

  //Simple Barron
  async getSimpleBarron(data) {
    let tl = [];
    const material = data.material;

    const cl = Math.sqrt(
      (material.mYoung * 1000000000) /
        (material.densidad * (1 - Math.pow(material.cPoison, 2)))
    );

    const fs =
      (Math.PI / (4 * Math.sqrt(3))) *
      cl *
      data.ancho *
      (Math.pow(1 / data.largo, 2) + Math.pow(1 / data.alto, 2));

    const fc =
      (Math.sqrt(3) * Math.pow(this.propagacion, 2)) /
      (Math.PI * cl * data.ancho);

    var ks;
    const cs =
      (768 * (1 - Math.pow(material.cPoison, 2))) /
      (Math.pow(Math.PI, 8) *
        material.mYoung *
        1000000000 *
        Math.pow(data.ancho, 3) *
        Math.pow(1 / Math.pow(data.alto, 2) + 1 / Math.pow(data.largo, 2), 2));

    const ms = material.densidad * data.ancho;

    for (let i = 0; i < this.f.length; i++) {
      tl.push();
      if (this.f[i] < fs) {
        ks = 4 * Math.PI * this.f[i] * this.densidad * this.propagacion * cs;
        tl[i] =
          10 * Math.log10(1 + 1 / Math.pow(ks, 2)) -
          10 * Math.log10(Math.log(1 + 1 / Math.pow(ks, 2)));
      }
      if (this.f[i] > fs && this.f[i] < fc) {
        tl[i] =
          10 *
            Math.log10(
              1 +
                Math.pow(
                  (Math.PI * this.f[i] * ms) /
                    (this.densidad * this.propagacion),
                  2
                )
            ) -
          5;
      }
      if (this.f[i] > fc) {
        tl[i] =
          10 *
            Math.log10(
              1 +
                Math.pow(
                  (Math.PI * fc * ms) / (this.densidad * this.propagacion),
                  2
                )
            ) +
          10 * Math.log10(material.factorN) +
          33.22 * Math.log10(this.f[i] / fc) -
          5.7;
      }
    }

    return await { tl: tl, ms: ms };
  }
  //Simple Sharp

  async getSimlpeSharp(data) {
    let tl = [];
    const material = data.material;

    const cl = Math.sqrt(
      (material.mYoung * 1000000000) /
        (material.densidad * (1 - Math.pow(material.cPoison, 2)))
    );

    const fs =
      (Math.PI / (4 * Math.sqrt(3))) *
      cl *
      data.ancho *
      (Math.pow(1 / data.largo, 2) + Math.pow(1 / data.alto, 2));

    const fc =
      (Math.sqrt(3) * Math.pow(this.propagacion, 2)) /
      (Math.PI * cl * data.ancho);

    var ks;
    const cs =
      (768 * (1 - Math.pow(material.cPoison, 2))) /
      (Math.pow(Math.PI, 8) *
        material.mYoung *
        1000000000 *
        Math.pow(data.ancho, 3) *
        Math.pow(1 / Math.pow(data.alto, 2) + 1 / Math.pow(data.largo, 2), 2));

    const ms = material.densidad * data.ancho;

    for (let i = 0; i < this.f.length; i++) {
      tl.push();
      if (this.f[i] < 1.5 * fs) {
        tl[i] = 20 * Math.log10(this.f[i] * ms) - 48;
      }
      if (this.f[i] > 1.5 * fs && this.f[i] < fc) {
        tl[i] =
          10 *
            Math.log10(
              1 +
                Math.pow(
                  (Math.PI * this.f[i] * ms) /
                    (this.densidad * this.propagacion),
                  2
                )
            ) -
          5.5;
      }
      if (this.f[i] > fc) {
        tl[i] =
          10 *
            Math.log10(
              1 +
                Math.pow(
                  (Math.PI * this.f[i] * ms) /
                    (this.densidad * this.propagacion),
                  2
                )
            ) +
          10 * Math.log10((2 * material.factorN * this.f[i]) / (Math.PI * fc));
      }
    }

    return await { tl: tl, ms: ms };
  }

  async getSimple(data) {
    if (this.metodo == "sharp") return await this.getSimlpeSharp(data);
    else if (this.metodo == "barron") return await this.getSimpleBarron(data);
  }

  //Dobles Barron

  async getDobleBarron(data) {
    let tl = [];

    const dataM1 = await this.getSimple({
      material: data.material,
      largo: data.largo,
      ancho: data.espesor,
      alto: data.alto,
    });
    const dataM2 = await this.getSimple({
      material: data.material2,
      largo: data.largo,
      ancho: data.espesor2,
      alto: data.alto,
    });

    const f1 =
      (this.densidad * this.propagacion) / (Math.PI * (dataM1.ms + dataM2.ms));

    //paso3
    const f0 =
      (this.propagacion / (2 * Math.PI)) *
      Math.sqrt(
        (this.densidad / data.distancia) * (1 / dataM1.ms + 1 / dataM2.ms)
      );

    //paso4
    const f3 = this.propagacion / (2 * Math.PI * data.distancia);

    for (let i = 0; i < this.f.length; i++) {
      tl.push();

      if (this.f[i] > f1 && this.f[i] < f0) {
        tl[i] =
          20 * Math.log10(dataM1.ms + dataM2.ms) +
          20 * Math.log10(this.f[i]) -
          47.3;
      }
      if (this.f[i] > f0 && this.f[i] < f3) {
        tl[i] =
          dataM1.tl[i] +
          dataM2.tl[i] +
          20 *
            Math.log10(
              (4 * Math.PI * this.f[i] * data.distancia) / this.propagacion
            );
      }
      if (this.f[i] > f3) {
        tl[i] =
          dataM1.tl[i] +
          dataM2.tl[i] +
          10 * Math.log10(4 / (1 + 2 / data.material1.coefInterno));
      }
    }

    return { tl: tl };
  }

  //Dobles De Sharp.
  async getDobleSharp(data) {
    let tl = [];

    const material = data.material;

    const dataM3 = await this.getSimple({
      material: data.material,
      largo: data.largo,
      ancho: data.espesor,
      alto: data.alto,
    });
    const dataM4 = await this.getSimple({
      material: data.material2,
      largo: data.largo,
      ancho: data.espesor2,
      alto: data.alto,
    });

    const f0 =
      (1 / (2 * Math.PI)) *
      Math.sqrt(
        (1.8 *
          this.densidad *
          Math.pow(this.propagacion, 2) *
          (dataM3.ms + dataM4.ms)) /
          (data.distancia * dataM3.ms * dataM4.ms)
      );

    const fl = this.propagacion / (2 * Math.PI * data.distancia);

    const cl1 = Math.sqrt(
      (data.material.mYoung * 1000000000) /
        (data.material.densidad * (1 - Math.pow(data.material.cPoison, 2)))
    );

    const cl2 = Math.sqrt(
      (data.material2.mYoung * 1000000000) /
        (data.material2.densidad * (1 - Math.pow(data.material2.cPoison, 2)))
    );

    const fc3 = (0.55 * Math.pow(this.propagacion, 2)) / (cl1 * data.espesor);

    const fc4 = (0.55 * Math.pow(this.propagacion, 2)) / (cl2 * data.espesor2);

    let fc1 = 0;
    let fc2 = 0;
    let ms1 = 1;
    let ms2 = 0;

    if (fc3 < fc4) {
      fc1 = fc3;

      fc2 = fc4;

      ms1 = dataM3.ms;

      ms2 = dataM4.ms;
    } else {
      fc1 = fc4;

      fc2 = fc3;

      ms1 = dataM3.ms;

      ms2 = dataM4.ms;
    }

    const tlA = 20 * Math.log10((ms1 + ms2) * f0) - 48;

    const tlB1 = tlA + 20 * Math.log10(fc1 / f0) - 6;

    const tlB2 =
      10 *
        Math.log10(
          Math.pow(ms1, 2) *
            data.constanteB *
            Math.pow(fc2, 3) *
            Math.pow(1 + (ms2 * Math.sqrt(fc1)) / (ms1 * Math.sqrt(fc2)), 2)
        ) -
      77;

    let tlB = 0;

    if (tlB1 > tlB2) {
      tlB = tlB1;
    } else if (tlB1 < tlB2) {
      tlB = tlB2;
    }

    let tlC = 0;

    const tlC1 =
      tlB +
      6 +
      10 * Math.log10(data.material2.factorN) +
      5 * Math.log10(data.material.factorN);

    const tlC2 = tlB + 6 + 10 * Math.log10(data.material2.factorN);

    if (fc1 == fc2) {
      tlC = tlC1;
    } else {
      tlC = tlC2;
    }

    let fp = 0;

    for (let i = 1; i <= 4000; i++) {
      if (i > f0 && i < 0.5 * fc1) {
        let ag =
          20 * Math.log10(ms1 * i) -
          48 +
          20 * Math.log10(ms2 * i) -
          48 +
          20 * Math.log10(i * data.distancia) -
          29 -
          (10 *
            Math.log10(
              Math.pow(ms1, 2) *
                fc2 *
                data.constanteB *
                Math.pow(
                  i * (1 + (ms2 * Math.sqrt(fc1)) / (ms1 * Math.sqrt(fc2))),
                  2
                )
            ) -
            77 +
            4);

        if (ag >= 0) {
          fp = i;

          break;
        }
      }
    }

    //regiones al pelo

    for (let i = 0; i < this.f.length; i++) {
      tl.push();

      if (this.f[i] < f0) {
        tl[i] = 20 * Math.log10(ms1 + ms2) + 20 * Math.log10(this.f[i]) - 48;
      }
      if (this.f[i] > f0 && this.f[i] < fp) {
        tl[i] =
          20 * Math.log10(ms1 * this.f[i]) -
          48 +
          (20 * Math.log10(ms2 * this.f[i]) - 48) +
          20 * Math.log10(this.f[i] * data.distancia) -
          29;
      }
      if (this.f[i] > fp && this.f[i] < 0.5 * fc1) {
        tl[i] =
          10 *
            Math.log10(
              Math.pow(ms1, 2) *
                fc2 *
                data.constanteB *
                Math.pow(
                  this.f[i] *
                    (1 + (ms2 * Math.sqrt(fc1)) / (ms1 * Math.sqrt(fc2))),
                  2
                )
            ) -
          77 +
          4;
      }
      if (this.f[i] > 0.5 * fc1 && this.f[i] < fc2) {
        tl[i] =
          Math.log10(this.f[i] / (0.5 * fc1)) *
            ((tlC - tlB) / Math.log10(fc2 / (0.5 * fc1))) +
          tlB;
      }
      if (this.f[i] > fc2) {
        tl[i] = 50 * Math.log10((Math.pow(10, tlC / 50) / fc2) * this.f[i]);
      }
    }

    return { tl: tl };
  }

  async getDoble(data) {
    if (this.metodo == "sharp") return await this.getDobleSharp(data);
    else if (this.metodo == "barron") return await this.getDobleBarron(data);
  }

  async getCompuesta(data) {
    let tl = [];
    let response;

    for (let i = 0; i < data.length; i++) {
      let form = data[i];
      if (form.contenido) {
        let relativo = data[parseInt(form.contenido)];

        if (relativo.porcentaje) {
          form.porcentaje =
            (relativo.porcentaje * form.largo * form.alto) /
            (relativo.largo * relativo.alto);
          relativo.porcentaje -= form.porcentaje;
        } else {
          form.porcentaje =
            (form.largo * form.alto) / (relativo.largo * relativo.alto);
          relativo.porcentaje = 1 - form.porcentaje;
        }
      } else {
        if (!form.porcentaje) {
          form.porcentaje = 1;
        }
      }
    }

    for (let i = 0; i < data.length; i++) {
      let form = data[i];
      if (form.type == this.types.simple) response = await this.getSimple(form);
      if (form.type == this.types.doble) response = await this.getDoble(form);
      await tl.push({ res: response.tl, porcentaje: form.porcentaje });
      form.porcentaje = null;
    }
    return this.temp(tl);
  }

  /**Impresión TR */
  saveTR(tr: FormGroup) {
    if (tr.valid) {
      this.currentCara = this.caras.derecha;
      this.currentDatos = this.datosPresion.sp;
      this.sabine = 0.16 * tr.get("volumen").value;
    } else {
      alert(
        "Revise que todos los datos esten bien y que no existen campos vacíos"
      );
    }
  }

  saveSP(sp: FormArray) {
    /*Calcular el tr queda como this.tr.value*/
    if (sp.valid) {
      if (this.seleccionado != "" && this.seleccionado != null) {
        switch (this.seleccionado) {
          case "NO":
            for (let i = 0; i < this.tl.length; i++) {
              let element =
                sp.value[i] -
                this.tl[i] +
                10 *
                  Math.log10(this.absor / (this.sabine / this.tr.value._tr[i]));
              if (isNaN(element)) element = 0;
              if (element < 0){
                this.datosBarras.push(null);
              }
              else {
                this.datosBarras.push(element);
              }
            }
            this.enableStep(this.steps.resultados);
            setTimeout(() => {
              this.resultado_pagina();
            });
            break;
          case "SI":
            if (this.fuenteSonora != "" && this.fuenteSonora != null) {
              if (this.fuenteSonora == "ruidoFondo") {
                for (let i = 0; i < this.tl.length; i++) {
                  let element =
                    sp.value[i] -
                    this.tl[i] +
                    10 *
                      Math.log10(
                        this.absor / (this.sabine / this.tr.value._tr[i])
                      ) -
                    3.0;
                  if (isNaN(element)) element = 0;
                  if (element < 0){
                    this.datosBarras.push(null);
                  }
                  else {
                    this.datosBarras.push(element);
                  }
                }
                this.enableStep(this.steps.resultados);
                setTimeout(() => {
                  this.resultado_pagina();
                });
              } else {
                for (let i = 0; i < this.tl.length; i++) {
                  let element =
                    sp.value[i] -
                    this.tl[i] +
                    10 *
                      Math.log10(
                        this.absor / (this.sabine / this.tr.value._tr[i])
                      ) -
                    1.5;
                  if (isNaN(element)) element = 0;
                  if (element < 0){
                    this.datosBarras.push(null);
                  }
                  else {
                    this.datosBarras.push(element);
                  }
                }
                this.enableStep(this.steps.resultados);
                setTimeout(() => {
                  this.resultado_pagina();
                });
              }
            } else {
              alert("Seleccione su fuente de ruido");
            }
            break;
        }
      } else {
        alert("Selecione si su particion colinda con fachada o no");
      }
    } else {
      alert(
        "Revise que todos los datos esten bien y que no existen campos vacíos"
      );
    }
  }
  /*Guarda los sp por cara en el objeto spCaras*/
  saveSPCara(sp: FormArray, cara) {
    if (sp.valid) {
      if (this.seleccionado != "" && this.seleccionado != null) {
        if (
          (this.fuenteSonora != "" && this.fuenteSonora != null) ||
          this.seleccionado == "NO"
        ) {
          this.paramCaras[cara].spIn = sp.value;
          this.paramCaras[cara].fachada = this.seleccionado;
          this.paramCaras[cara].fuenteSonora = this.fuenteSonora;
          this.paramCaras[cara].guardado = true;
          this.seleccionado = "";
          this.fuenteSonora = "";

          for (let i = 0; i < this.f.length; i++) {
            sp.controls[i].setValue(null);
          }
        } else {
          alert("Seleccione su fuente ruido");
        }
      } else {
        alert("Seleccione si su particion colinda con fachada o no");
      }
    } else {
      alert("valide que todo este bien");
    }
  }

  async splCara(cara) {
    switch (cara.fachada) {
      case "NO":
        for (let i = 0; i < cara.tl.length; i++) {
          let element =
            cara.spIn[i] -
            cara.tl[i] +
            10 * Math.log10(cara.absor / (this.sabine / this.tr.value._tr[i]));
          if (isNaN(element)) element = 0;
          if (element < 0){
            cara.sp[i] = null;
          }
          else {
            cara.sp[i] = element;
          }
        }

        break;

      case "SI":
        if (cara.fuenteSonora == "ruidoFondo") {
          for (let i = 0; i < cara.tl.length; i++) {
            let element =
              cara.spIn[i] -
              cara.tl[i] +
              10 *
                Math.log10(cara.absor / (this.sabine / this.tr.value._tr[i])) -
              3.0;
            if (isNaN(element)) element = 0;
            if (element < 0){
              cara.sp[i] = null;
            }
            else {
              cara.sp[i] = element;
            }
          }
        } else {
          for (let i = 0; i < cara.tl.length; i++) {
            let element =
              cara.spIn[i] -
              cara.tl[i] +
              10 *
                Math.log10(cara.absor / (this.sabine / this.tr.value._tr[i])) -
              1.5;
            if (isNaN(element)) element = 0;
            if (element < 0){
              cara.sp[i] = null;
            }
            else {
              cara.sp[i] = element;
            }
          }
          break;
        }
    }
  }

  lpTotal() {
    let lp = 0;

    for (let i = 0; i < this.tl.length; i++) {
      if (this.view.particion == this.currentView) {
        const element = Math.pow(10, this.datosBarras[i] / 10);
        lp += element;

        this.difNiveles[i] = this.sp.value[i] - this.datosBarras[i];

        this.difnivelesDnt[i] =
          this.sp.value[i] -
          this.datosBarras[i] +
          10 * Math.log10(this.tr.value._tr[i] / 0.5);
      } else {
        if (this.inferior) {
          this.splFrecuencias[i] =
            10 *
            Math.log10(
              Math.pow(10, this.paramCaras.Derecha.sp[i] / 10) +
                Math.pow(10, this.paramCaras.Izquierda.sp[i] / 10) +
                Math.pow(10, this.paramCaras.Frontal.sp[i] / 10) +
                Math.pow(10, this.paramCaras.Trasera.sp[i] / 10) +
                Math.pow(10, this.paramCaras.Superior.sp[i] / 10) +
                Math.pow(10, this.paramCaras.Inferior.sp[i] / 10)
            );
          this.paramCaras.Inferior.difNiveles[i] =
            this.paramCaras.Inferior.spIn[i] - this.paramCaras.Inferior.sp[i];
        } else {
          this.splFrecuencias[i] =
            10 *
            Math.log10(
              Math.pow(10, this.paramCaras.Derecha.sp[i] / 10) +
                Math.pow(10, this.paramCaras.Izquierda.sp[i] / 10) +
                Math.pow(10, this.paramCaras.Frontal.sp[i] / 10) +
                Math.pow(10, this.paramCaras.Trasera.sp[i] / 10) +
                Math.pow(10, this.paramCaras.Superior.sp[i] / 10)
            );
        }

        this.paramCaras.Derecha.difNiveles[i] =
          this.paramCaras.Derecha.spIn[i] - this.paramCaras.Derecha.sp[i];

        this.paramCaras.Izquierda.difNiveles[i] =
          this.paramCaras.Izquierda.spIn[i] - this.paramCaras.Izquierda.sp[i];

        this.paramCaras.Frontal.difNiveles[i] =
          this.paramCaras.Frontal.spIn[i] - this.paramCaras.Frontal.sp[i];

        this.paramCaras.Trasera.difNiveles[i] =
          this.paramCaras.Trasera.spIn[i] - this.paramCaras.Trasera.sp[i];

        this.paramCaras.Superior.difNiveles[i] =
          this.paramCaras.Superior.spIn[i] - this.paramCaras.Superior.sp[i];

        this.paramCaras.Inferior.difNiveles[i] =
          this.paramCaras.Inferior.spIn[i] - this.paramCaras.Inferior.sp[i];

        const mar = Math.pow(10, this.splFrecuencias[i] / 10);

        this.paramCaras.Derecha.dnt1[i] =
          this.paramCaras.Derecha.spIn[i] -
          this.paramCaras.Derecha.sp[i] +
          10 * Math.log10(this.tr.value._tr[i] / 0.5);

        this.paramCaras.Frontal.dnt1[i] =
          this.paramCaras.Frontal.spIn[i] -
          this.paramCaras.Frontal.sp[i] +
          10 * Math.log10(this.tr.value._tr[i] / 0.5);

        this.paramCaras.Izquierda.dnt1[i] =
          this.paramCaras.Izquierda.spIn[i] -
          this.paramCaras.Izquierda.sp[i] +
          10 * Math.log10(this.tr.value._tr[i] / 0.5);

        this.paramCaras.Trasera.dnt1[i] =
          this.paramCaras.Trasera.spIn[i] -
          this.paramCaras.Trasera.sp[i] +
          10 * Math.log10(this.tr.value._tr[i] / 0.5);

        this.paramCaras.Superior.dnt1[i] =
          this.paramCaras.Superior.spIn[i] -
          this.paramCaras.Superior.sp[i] +
          10 * Math.log10(this.tr.value._tr[i] / 0.5);

        this.paramCaras.Inferior.dnt1[i] =
          this.paramCaras.Inferior.spIn[i] -
          this.paramCaras.Inferior.sp[i] +
          10 * Math.log10(this.tr.value._tr[i] / 0.5);

        lp += mar;
      }
    }
    this.and = 10 * Math.log10(lp);
  }

  splAll() {
    this.splCara(this.paramCaras.Derecha);
    this.splCara(this.paramCaras.Izquierda);
    this.splCara(this.paramCaras.Frontal);
    this.splCara(this.paramCaras.Trasera);
    this.splCara(this.paramCaras.Superior);
    if (this.inferior) this.splCara(this.paramCaras.Inferior);
    this.enableStep(this.steps.resultados);
    setTimeout(() => {
      this.resultado_pagina();
    });
  }

  async temp(data) {
    let tl = [];
    for (let i = 0; i < this.f.length; i++) {
      tl.push();
      let sum = 0;
      for (let j = 0; j < data.length; ++j) {
        sum += (data[j].porcentaje * 1) / Math.pow(10, 0.1 * data[j].res[i]);
      }
      tl[i] = 10 * Math.log10(1 / sum);
    }
    return await tl;
  }

  Rw(arrTL: Array<number>) {
    let r1W;

    let diferencia;

    let cont = 0;

    do {
      diferencia = 0;
      for (let i = 0; i < arrTL.length; i++) {
        if (this.refVal[i] && this.refVal[i] - arrTL[i] - cont > 0) {
          diferencia += this.refVal[i] - arrTL[i] - cont;
        }
      }
      ++cont;
    } while (diferencia > 32);

    let desplazado = [];

    for (let i = 0; i < arrTL.length; i++) {
      desplazado.push();

      if (this.refVal[i]) {
        desplazado[i] = this.refVal[i] - cont + 1;
      }
    }

    r1W = 52 - (cont - 1);

    return { desplazado: desplazado, r1W: r1W };
  }

  espectros(arrEspectro: Array<number>, arrTl: Array<number>) {
    let espectro = [];

    let suma = 0;

    let diferencia1;

    diferencia1 = 0;

    for (let i = 0; i < arrTl.length; i++) {
      if (arrEspectro[i]) {
        diferencia1 = arrEspectro[i] - arrTl[i];

        espectro.push();

        espectro[i] = Math.pow(10, diferencia1 / 10);

        suma += espectro[i];
      }
    }

    const resultadoEspectro = -10 * Math.log10(suma);

    return Math.round(resultadoEspectro);
  }

  stC(arrTL: Array<number>) {
    let resultadoStc;

    let res_Stc = [];

    let diferencia2;

    let cont = 0;

    let difMax = -1000;

    let graficoStc = [];

    let nPP = 0;

    diferencia2 = 0;

    for (let i = 0; i < arrTL.length; i++) {
      if (this.stc[i]) {
        diferencia2 = this.stc[i] - arrTL[i];

        difMax = Math.max(difMax, diferencia2);
      }
    }

    const stc1 = 58 - difMax;

    for (let i = 0; i < arrTL.length; i++) {
      res_Stc.push();

      if (this.stc[i]) {
        diferencia2 = this.stc[i] - arrTL[i];

        res_Stc[i] = diferencia2 - (50 - stc1);

        nPP += res_Stc[i] > 0 ? res_Stc[i] : 0;

        cont += res_Stc[i] > 0 ? 1 : 0;
      }
    }

    if (nPP < 32) {
      resultadoStc = Math.round(stc1);
    } else {
      let ajuste = Math.ceil((nPP - 32) / cont);

      resultadoStc = Math.round(stc1 - ajuste);
    }

    for (let i = 0; i < arrTL.length; i++) {
      graficoStc.push();

      if (this.stc[i]) {
        graficoStc[i] = this.stc[i] - resultadoStc;
      }
    }
    return { resultadoStc: resultadoStc, graficoStc: graficoStc };
  }

  calc_D(difnivelesDnt: Array<number>) {
    this.dNt = 0;

    let diferenciaD;

    let cont = 0;

    do {
      diferenciaD = 0;

      for (let i = 0; i < difnivelesDnt.length; i++) {
        if (this.refVal[i] && this.refVal[i] - difnivelesDnt[i] - cont > 0) {
          diferenciaD += this.refVal[i] - difnivelesDnt[i] - cont;
        }
      }
      ++cont;
    } while (diferenciaD > 32);

    let desplazadoD = [];

    for (let i = 0; i < difnivelesDnt.length; i++) {
      desplazadoD.push();

      if (this.refVal[i]) {
        desplazadoD[i] = this.refVal[i] - cont + 1;
      }
    }

    this.dNt = 52 - (cont - 1);

    return { desplazado: desplazadoD, dNt: this.dNt };
  }

  espectrosDnt(arrEspectroDnt: Array<number>, difnivelesDnt: Array<number>) {
    let espectro = [];

    let suma = 0;

    let diferencia1;

    diferencia1 = 0;

    for (let i = 0; i < difnivelesDnt.length; i++) {
      if (arrEspectroDnt[i]) {
        diferencia1 = arrEspectroDnt[i] - difnivelesDnt[i];

        espectro.push();

        espectro[i] = Math.pow(10, diferencia1 / 10);

        suma += espectro[i];
      }
    }
  }

  plot() {
    /*traer un valor de un metodo*/
    const rw = this.Rw(this.tl);
    this.r1W = rw.r1W;
    const stc = this.stC(this.tl);
    this.resultadoStc = stc.resultadoStc;
    this.espectroCalculo = this.espectros(this.espectroC, this.tl) - this.r1W;
    this.espectroCtrCalculo =
      this.espectros(this.espectroCtr, this.tl) - this.r1W;
    if (this.lineChart) {
      this.lineChart.clear();
      this.lineChart.destroy();
    }
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: "line",
      data: {
        labels: this.f,
        datasets: [
          {
            label: "Pérdida por transmisión",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,255,128,0.4)",
            borderColor: "rgba(0,255,128,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.tl,
            spanGaps: false,
          },
          {
            label: "Valor de referencia R.W",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,128,255,0.4)",
            borderColor: "rgba(0,128,255,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: rw.desplazado,
            spanGaps: false,
          },
          {
            label: "Valor de referencia S.T.C",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,0,0,0.4)",
            borderColor: "rgba(255,0,0,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: stc.graficoStc,
            spanGaps: false,
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Pérdida por transmisión (dB)",
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Frecuencia (Hz)",
              },
            },
          ],
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              var label = data.datasets[tooltipItem.datasetIndex].label || "";

              if (label) {
                label += ": ";
              }
              label += Math.round(tooltipItem.yLabel * 100) / 100;
              return label;
            },
          },
        },
      },
    });
  }

  plotAll(
    tlDerecha,
    tlIzquierda,
    tlFrontal,
    tlTrasera,
    tlSuperior,
    tlInferior
  ) {
    this.lineChart = new Chart(this.lineCanvas.nativeElement, {
      type: "line",
      data: {
        labels: this.f,
        datasets: [
          {
            label: "TL DERECHA",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,255,128,0.4)",
            borderColor: "rgba(0,255,128,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: tlDerecha,
            spanGaps: false,
          },
          {
            label: "TL IZQUIERDA",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,255,0,255)",
            borderColor: "rgba(255,255,0,255)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: tlIzquierda,
            spanGaps: false,
          },
          {
            label: " TL FRONTAL",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,128,255,0.4)",
            borderColor: "rgba(0,128,255,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: tlFrontal,
            spanGaps: false,
          },
          {
            label: "TL TRASERA",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,0,0,0.4)",
            borderColor: "rgba(255,0,0,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: tlTrasera,
            spanGaps: false,
          },
          {
            label: "TL SUPERIOR",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,153,153,0.4)",
            borderColor: "rgba(0,153,153,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: tlSuperior,
            spanGaps: false,
          },
          {
            label: "TL INFERIOR",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,0,153,0.4)",
            borderColor: "rgba(0,0,153,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: tlInferior,
            spanGaps: false,
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Pérdida por transmisión (dB)",
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Frecuencia (Hz)",
              },
            },
          ],
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              var label = data.datasets[tooltipItem.datasetIndex].label || "";

              if (label) {
                label += ": ";
              }
              label += Math.round(tooltipItem.yLabel * 100) / 100;
              return label;
            },
          },
        },
      },
    });
  }

  plot_n() {
    this.lineChartNc = new Chart(this.lineCanvasNc.nativeElement, {
      type: "line",
      data: {
        labels: this.f,
        datasets: [
          {
            label: "NC 15",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,153,153,0.4)",
            borderColor: "rgba(0,153,153,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.nC15,
            spanGaps: false,
          },
          {
            label: "NC 20",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,0,0,0.4)",
            borderColor: "rgba(255,0,0,0.4)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.nC20,
            spanGaps: false,
          },
          {
            label: "NC 25",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,255,0,0.4)",
            borderColor: "rgba(0,255,0,0.4)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.nC25,
            spanGaps: false,
          },
          {
            label: "NC 30",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(153,50,204,0.4)",
            borderColor: "rgba(153,50,204,0.4)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.nC30,
            spanGaps: false,
          },
          {
            label: "NC 35",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(184,134,11,0.4)",
            borderColor: "rgba(184,134,11,0.4)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.nC35,
            spanGaps: false,
          },
          {
            label: "NC 40",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(119,136,153,0.4)",
            borderColor: "rgba(119,136,153,0.4)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.nC40,
            spanGaps: false,
          },
          {
            label: "NC 45",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,10,255,0.4)",
            borderColor: "rgba(0,10,255,0.4)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.nC45,
            spanGaps: false,
          },
          {
            label: "NC 50",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,206,208,0.4)",
            borderColor: "rgba(0,206,208,0.4)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.nC50,
            spanGaps: false,
          },
          {
            label: "NC 55",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,0,0,0.4)",
            borderColor: "rgba(255,0,0,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.nC55,
            spanGaps: false,
          },
          {
            label: "NC 60",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,128,255,0.4)",
            borderColor: "rgba(0,128,255,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.nC60,
            spanGaps: false,
          },
          {
            label: "NC 65",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,255,0,255)",
            borderColor: "rgba(255,255,0,255)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.nC65,
            spanGaps: false,
          },
          {
            label: "NC 70",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,255,128,0.4)",
            borderColor: "rgba(0,255,128,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.nC70,
            spanGaps: false,
          },
          {
            label: "PROMEDIO",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,0,0,0.4)",
            borderColor: "rgba(0,0,0,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data:
              this.view.particion == this.currentView
                ? this.datosBarras
                : this.splFrecuencias,
            spanGaps: false,
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Nivel de presión sonora (dB)",
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Frecuencia (Hz)",
              },
            },
          ],
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              var label = data.datasets[tooltipItem.datasetIndex].label || "";

              if (label) {
                label += ": ";
              }
              label += Math.round(tooltipItem.yLabel * 100) / 100;
              return label;
            },
          },
        },
      },
    });
  }

  plot_LyR(tlDerecha, tlIzquierda) {
    let rw = this.Rw(tlDerecha);
    this.paramCaras.Derecha.rwRes = rw.desplazado;
    this.paramCaras.Derecha.r1W = rw.r1W;
    this.paramCaras.Derecha.c =
      this.espectros(this.espectroC, tlDerecha) - rw.r1W;
    this.paramCaras.Derecha.ctr =
      this.espectros(this.espectroCtr, tlDerecha) - rw.r1W;
    rw = this.Rw(tlIzquierda);
    this.paramCaras.Izquierda.rwRes = rw.desplazado;
    this.paramCaras.Izquierda.r1W = rw.r1W;
    this.paramCaras.Izquierda.c =
      this.espectros(this.espectroC, tlIzquierda) - rw.r1W;
    this.paramCaras.Izquierda.ctr =
      this.espectros(this.espectroCtr, tlIzquierda) - rw.r1W;
    this.lineChartRL = new Chart(this.lineCanvasLR.nativeElement, {
      type: "line",
      data: {
        labels: this.f,
        datasets: [
          {
            label: "TL DERECHA",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,255,128,0.4)",
            borderColor: "rgba(0,255,128,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: tlDerecha,
            spanGaps: false,
          },
          {
            label: "TL IZQUIERDA",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,255,0,255)",
            borderColor: "rgba(255,255,0,255)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: tlIzquierda,
            spanGaps: false,
          },
          {
            label: "VALOR DESPLAZADO R.W DERECHA",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,128,255,0.4)",
            borderColor: "rgba(0,128,255,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Derecha.rwRes,
            spanGaps: false,
          },
          {
            label: "VALOR DESPLAZADO R.W IZQUIERDA",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,0,0,0.4)",
            borderColor: "rgba(255,0,0,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Izquierda.rwRes,
            spanGaps: false,
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Indíce de reducción sonora (dB)",
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Frecuencia (Hz)",
              },
            },
          ],
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              var label = data.datasets[tooltipItem.datasetIndex].label || "";

              if (label) {
                label += ": ";
              }
              label += Math.round(tooltipItem.yLabel * 100) / 100;
              return label;
            },
          },
        },
      },
    });
  }

  plot_FyB(tlFrontal, tlTrasera) {
    let rw = this.Rw(tlFrontal);
    this.paramCaras.Frontal.rwRes = rw.desplazado;
    this.paramCaras.Frontal.r1W = rw.r1W;
    this.paramCaras.Frontal.c =
      this.espectros(this.espectroC, tlFrontal) - rw.r1W;
    this.paramCaras.Frontal.ctr =
      this.espectros(this.espectroCtr, tlFrontal) - rw.r1W;
    rw = this.Rw(tlTrasera);
    this.paramCaras.Trasera.rwRes = rw.desplazado;
    this.paramCaras.Trasera.r1W = rw.r1W;
    this.paramCaras.Trasera.c =
      this.espectros(this.espectroC, tlTrasera) - rw.r1W;
    this.paramCaras.Trasera.ctr =
      this.espectros(this.espectroCtr, tlTrasera) - rw.r1W;
    this.lineChartFB = new Chart(this.lineCanvasFB.nativeElement, {
      type: "line",
      data: {
        labels: this.f,
        datasets: [
          {
            label: " TL FRONTAL",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,128,255,0.4)",
            borderColor: "rgba(0,128,255,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: tlFrontal,
            spanGaps: false,
          },
          {
            label: "TL TRASERA",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,0,0,0.4)",
            borderColor: "rgba(255,0,0,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: tlTrasera,
            spanGaps: false,
          },
          {
            label: "VALOR DESPLAZADO R.W FRONTAL",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,128,255,0.4)",
            borderColor: "rgba(0,128,255,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Frontal.rwRes,
            spanGaps: false,
          },
          {
            label: "VALOR DESPLAZADO R.W TRASERA",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,0,0,0.4)",
            borderColor: "rgba(255,0,0,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Trasera.rwRes,
            spanGaps: false,
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Indíce de reducción sonora (dB)",
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Frecuencia (Hz)",
              },
            },
          ],
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              var label = data.datasets[tooltipItem.datasetIndex].label || "";

              if (label) {
                label += ": ";
              }
              label += Math.round(tooltipItem.yLabel * 100) / 100;
              return label;
            },
          },
        },
      },
    });
  }

  plot_UyD(tlSuperior, tlInferior) {
    let rw = this.Rw(tlSuperior);
    this.paramCaras.Superior.rwRes = rw.desplazado;
    this.paramCaras.Superior.r1W = rw.r1W;
    this.paramCaras.Superior.c =
      this.espectros(this.espectroC, tlSuperior) - rw.r1W;
    this.paramCaras.Superior.ctr =
      this.espectros(this.espectroCtr, tlSuperior) - rw.r1W;
    if (tlInferior) {
      rw = this.Rw(tlInferior);
      this.paramCaras.Inferior.rwRes = rw.desplazado;
      this.paramCaras.Inferior.r1W = rw.r1W;
      this.paramCaras.Inferior.c =
        this.espectros(this.espectroC, tlInferior) - rw.r1W;
      this.paramCaras.Inferior.ctr =
        this.espectros(this.espectroCtr, tlInferior) - rw.r1W;
      this.lineChartUP = new Chart(this.lineCanvasUD.nativeElement, {
        type: "line",
        data: {
          labels: this.f,
          datasets: [
            {
              label: "TL SUPERIOR",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(0,153,153,0.4)",
              borderColor: "rgba(0,153,153,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(75,192,192,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(75,192,192,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: tlSuperior,
              spanGaps: false,
            },
            {
              label: "TL INFERIOR",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(0,0,153,0.4)",
              borderColor: "rgba(0,0,153,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(75,192,192,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(75,192,192,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: tlInferior,
              spanGaps: false,
            },
            {
              label: "VALOR DESPLAZADO R.W SUPERIOR",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(0,128,255,0.4)",
              borderColor: "rgba(0,128,255,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(75,192,192,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(75,192,192,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: this.paramCaras.Superior.rwRes,
              spanGaps: false,
            },
            {
              label: "VALOR DESPLAZADO R.W INFERIOR",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(255,0,0,0.4)",
              borderColor: "rgba(255,0,0,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(75,192,192,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(75,192,192,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: this.paramCaras.Inferior.rwRes,
              spanGaps: false,
            },
          ],
        },
        options: {
          scales: {
            yAxes: [
              {
                scaleLabel: {
                  display: true,
                  labelString: "Indíce de reducción sonora (dB)",
                },
              },
            ],
            xAxes: [
              {
                scaleLabel: {
                  display: true,
                  labelString: "Frecuencia (Hz)",
                },
              },
            ],
          },
          tooltips: {
            callbacks: {
              label: function (tooltipItem, data) {
                var label = data.datasets[tooltipItem.datasetIndex].label || "";

                if (label) {
                  label += ": ";
                }
                label += Math.round(tooltipItem.yLabel * 100) / 100;
                return label;
              },
            },
          },
        },
      });
    } else {
      this.lineChartUP = new Chart(this.lineCanvasUD.nativeElement, {
        type: "line",
        data: {
          labels: this.f,
          datasets: [
            {
              label: "TL SUPERIOR",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(0,153,153,0.4)",
              borderColor: "rgba(0,153,153,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(75,192,192,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(75,192,192,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: tlSuperior,
              spanGaps: false,
            },
            {
              label: "VALOR DESPLAZADO R.W SUPERIOR",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(0,128,255,0.4)",
              borderColor: "rgba(0,128,255,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(75,192,192,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(75,192,192,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: this.paramCaras.Superior.rwRes,
              spanGaps: false,
            },
          ],
        },
        options: {
          scales: {
            yAxes: [
              {
                scaleLabel: {
                  display: true,
                  labelString: "Indíce de reducción sonora (dB)",
                },
              },
            ],
            xAxes: [
              {
                scaleLabel: {
                  display: true,
                  labelString: "Frecuencia (Hz)",
                },
              },
            ],
          },
          tooltips: {
            callbacks: {
              label: function (tooltipItem, data) {
                var label = data.datasets[tooltipItem.datasetIndex].label || "";

                if (label) {
                  label += ": ";
                }
                label += Math.round(tooltipItem.yLabel * 100) / 100;
                return label;
              },
            },
          },
        },
      });
    }
  }

  plot_LyR_Stc(tlDerecha, tlIzquierda) {
    let stc = this.stC(tlDerecha);
    this.paramCaras.Derecha.resultadoStc = stc.resultadoStc;
    this.paramCaras.Derecha.arrayStc = stc.graficoStc;
    stc = this.stC(tlIzquierda);
    this.paramCaras.Izquierda.resultadoStc = stc.resultadoStc;
    this.paramCaras.Izquierda.arrayStc = stc.graficoStc;
    this.lineChartRLstc = new Chart(this.lineCanvasLRstc.nativeElement, {
      type: "line",
      data: {
        labels: this.f,
        datasets: [
          {
            label: "TL DERECHA",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,255,128,0.4)",
            borderColor: "rgba(0,255,128,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: tlDerecha,
            spanGaps: false,
          },
          {
            label: "TL IZQUIERDA",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,255,0,255)",
            borderColor: "rgba(255,255,0,255)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: tlIzquierda,
            spanGaps: false,
          },
          {
            label: "VALOR DESPLAZADO S.T.C DERECHA",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,128,255,0.4)",
            borderColor: "rgba(0,128,255,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Derecha.arrayStc,
            spanGaps: false,
          },
          {
            label: "VALOR DESPLAZADO S.T.C IZQUIERDA",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,0,0,0.4)",
            borderColor: "rgba(255,0,0,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Izquierda.arrayStc,
            spanGaps: false,
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Pérdida por transmisión (dB)",
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Frecuencia (Hz)",
              },
            },
          ],
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              var label = data.datasets[tooltipItem.datasetIndex].label || "";

              if (label) {
                label += ": ";
              }
              label += Math.round(tooltipItem.yLabel * 100) / 100;
              return label;
            },
          },
        },
      },
    });
  }

  plot_FyB_Stc(tlFrontal, tlTrasera) {
    let stc = this.stC(tlFrontal);
    this.paramCaras.Frontal.resultadoStc = stc.resultadoStc;
    this.paramCaras.Frontal.arrayStc = stc.graficoStc;
    stc = this.stC(tlTrasera);
    this.paramCaras.Trasera.resultadoStc = stc.resultadoStc;
    this.paramCaras.Trasera.arrayStc = stc.graficoStc;
    this.lineChartFBstc = new Chart(this.lineCanvasFBstc.nativeElement, {
      type: "line",
      data: {
        labels: this.f,
        datasets: [
          {
            label: " TL FRONTAL",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,128,255,0.4)",
            borderColor: "rgba(0,128,255,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: tlFrontal,
            spanGaps: false,
          },
          {
            label: "TL TRASERA",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,0,0,0.4)",
            borderColor: "rgba(255,0,0,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: tlTrasera,
            spanGaps: false,
          },
          {
            label: "VALOR DESPLAZADO S.T.C FRONTAL",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,128,255,0.4)",
            borderColor: "rgba(0,128,255,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Frontal.arrayStc,
            spanGaps: false,
          },
          {
            label: "VALOR DESPLAZADO S.T.C TRASERA",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,0,0,0.4)",
            borderColor: "rgba(255,0,0,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Trasera.arrayStc,
            spanGaps: false,
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Pérdida por transmisión (dB)",
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Frecuencia (Hz)",
              },
            },
          ],
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              var label = data.datasets[tooltipItem.datasetIndex].label || "";

              if (label) {
                label += ": ";
              }
              label += Math.round(tooltipItem.yLabel * 100) / 100;
              return label;
            },
          },
        },
      },
    });
  }

  plot_UyD_Stc(tlSuperior, tlInferior) {
    let stc = this.stC(tlSuperior);
    this.paramCaras.Superior.resultadoStc = stc.resultadoStc;
    this.paramCaras.Superior.arrayStc = stc.graficoStc;
    if (tlInferior) {
      stc = this.stC(tlInferior);
      this.paramCaras.Inferior.resultadoStc = stc.resultadoStc;
      this.paramCaras.Inferior.arrayStc = stc.graficoStc;
      this.lineChartUPstc = new Chart(this.lineCanvasUDstc.nativeElement, {
        type: "line",
        data: {
          labels: this.f,
          datasets: [
            {
              label: "TL SUPERIOR",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(0,153,153,0.4)",
              borderColor: "rgba(0,153,153,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(75,192,192,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(75,192,192,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: tlSuperior,
              spanGaps: false,
            },
            {
              label: "TL INFERIOR",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(0,0,153,0.4)",
              borderColor: "rgba(0,0,153,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(75,192,192,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(75,192,192,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: tlInferior,
              spanGaps: false,
            },
            {
              label: "VALOR DESPLAZADO S.T.C SUPERIOR",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(0,128,255,0.4)",
              borderColor: "rgba(0,128,255,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(75,192,192,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(75,192,192,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: this.paramCaras.Superior.arrayStc,
              spanGaps: false,
            },
            {
              label: "VALOR DESPLAZADO S.T.C INFERIOR",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(0,0,0,0.4)",
              borderColor: "rgba(255,0,0,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(75,192,192,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(75,192,192,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: this.paramCaras.Inferior.arrayStc,
              spanGaps: false,
            },
          ],
        },
        options: {
          scales: {
            yAxes: [
              {
                scaleLabel: {
                  display: true,
                  labelString: "Pérdida por transmisión (dB)",
                },
              },
            ],
            xAxes: [
              {
                scaleLabel: {
                  display: true,
                  labelString: "Frecuencia (Hz)",
                },
              },
            ],
          },
          tooltips: {
            callbacks: {
              label: function (tooltipItem, data) {
                var label = data.datasets[tooltipItem.datasetIndex].label || "";

                if (label) {
                  label += ": ";
                }
                label += Math.round(tooltipItem.yLabel * 100) / 100;
                return label;
              },
            },
          },
        },
      });
    } else {
      this.lineChartUPstc = new Chart(this.lineCanvasUDstc.nativeElement, {
        type: "line",
        data: {
          labels: this.f,
          datasets: [
            {
              label: "TL SUPERIOR",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(0,153,153,0.4)",
              borderColor: "rgba(0,153,153,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(75,192,192,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(75,192,192,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: tlSuperior,
              spanGaps: false,
            },
            {
              label: "VALOR DESPLAZADO S.T.C SUPERIOR",
              fill: false,
              lineTension: 0.1,
              backgroundColor: "rgba(0,128,255,0.4)",
              borderColor: "rgba(0,128,255,1)",
              borderCapStyle: "butt",
              borderDash: [],
              borderDashOffset: 0.0,
              borderJoinStyle: "miter",
              pointBorderColor: "rgba(75,192,192,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(75,192,192,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 2,
              pointRadius: 1,
              pointHitRadius: 10,
              data: this.paramCaras.Superior.arrayStc,
              spanGaps: false,
            },
          ],
        },
        options: {
          scales: {
            yAxes: [
              {
                scaleLabel: {
                  display: true,
                  labelString: "Pérdida por transmisión (dB)",
                },
              },
            ],
            xAxes: [
              {
                scaleLabel: {
                  display: true,
                  labelString: "Frecuencia (Hz)",
                },
              },
            ],
          },
          tooltips: {
            callbacks: {
              label: function (tooltipItem, data) {
                var label = data.datasets[tooltipItem.datasetIndex].label || "";

                if (label) {
                  label += ": ";
                }
                label += Math.round(tooltipItem.yLabel * 100) / 100;
                return label;
              },
            },
          },
        },
      });
    }
  }

  plot_Dnt() {
    this.lpTotal();
    const dnt1 = this.calc_D(this.difnivelesDnt);
    this.lineChartDnt = new Chart(this.lineCanvasDnT.nativeElement, {
      type: "line",
      data: {
        labels: this.f,
        datasets: [
          {
            label: "Diferencia de nivel estandarizada",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,255,128,0.4)",
            borderColor: "rgba(0,255,128,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.difnivelesDnt,
            spanGaps: false,
          },
          {
            label: "Curva de referencia desplazada",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,128,255,0.4)",
            borderColor: "rgba(0,128,255,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: dnt1.desplazado,
            spanGaps: false,
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Pérdida por transmisión (dB)",
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Frecuencia (Hz)",
              },
            },
          ],
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              var label = data.datasets[tooltipItem.datasetIndex].label || "";

              if (label) {
                label += ": ";
              }
              label += Math.round(tooltipItem.yLabel * 100) / 100;
              return label;
            },
          },
        },
      },
    });
  }

  plot_D() {
    const D = this.lpTotal();
    this.lineChartD = new Chart(this.lineCanvasD.nativeElement, {
      type: "line",
      data: {
        labels: this.f,
        datasets: [
          {
            label: "Diferencia de nivel normalizada aparente",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,255,128,0.4)",
            borderColor: "rgba(0,255,128,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.difNiveles,
            spanGaps: false,
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Pérdida por transmisión (dB)",
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Frecuencia (Hz)",
              },
            },
          ],
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              var label = data.datasets[tooltipItem.datasetIndex].label || "";

              if (label) {
                label += ": ";
              }
              label += Math.round(tooltipItem.yLabel * 100) / 100;
              return label;
            },
          },
        },
      },
    });
  }

  plot_Dproyecto() {
    const D = this.lpTotal();
    this.lineChartProD = new Chart(this.lineCanvasProD.nativeElement, {
      type: "line",
      data: {
        labels: this.f,
        datasets: [
          {
            label: "Diferencia de nivel normalizada aparente derecha",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,255,128,0.4)",
            borderColor: "rgba(0,255,128,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Derecha.difNiveles,
            spanGaps: false,
          },
          {
            label: "Diferencia de nivel normalizada aparente izquierda",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,255,0,255)",
            borderColor: "rgba(255,255,0,255)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Izquierda.difNiveles,
            spanGaps: false,
          },
          {
            label: "Diferencia de nivel normalizada aparente frontal",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,128,255,0.4)",
            borderColor: "rgba(0,128,255,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Frontal.difNiveles,
            spanGaps: false,
          },
          {
            label: "Diferencia de nivel normalizada aparente trasera",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,0,0,0.4)",
            borderColor: "rgba(255,0,0,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Trasera.difNiveles,
            spanGaps: false,
          },
          {
            label: "Diferencia de nivel normalizada aparente superior",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,153,153,0.4)",
            borderColor: "rgba(0,153,153,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Superior.difNiveles,
            spanGaps: false,
          },
          {
            label: "Diferencia de nivel normalizada aparente inferior",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,0,153,0.4)",
            borderColor: "rgba(0,0,153,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Inferior.difNiveles,
            spanGaps: false,
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Pérdida por transmisión (dB)",
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Frecuencia (Hz)",
              },
            },
          ],
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              var label = data.datasets[tooltipItem.datasetIndex].label || "";

              if (label) {
                label += ": ";
              }
              label += Math.round(tooltipItem.yLabel * 100) / 100;
              return label;
            },
          },
        },
      },
    });
  }

  plot_DNTproyecto() {
    this.lpTotal();
    let dNtValue;
    dNtValue = this.calc_D(this.paramCaras.Derecha.dnt1);
    this.paramCaras.Derecha.dNt = dNtValue.dNt;
    dNtValue = this.calc_D(this.paramCaras.Izquierda.dnt1);
    this.paramCaras.Izquierda.dNt = dNtValue.dNt;
    dNtValue = this.calc_D(this.paramCaras.Frontal.dnt1);
    this.paramCaras.Frontal.dNt = dNtValue.dNt;
    dNtValue = this.calc_D(this.paramCaras.Trasera.dnt1);
    this.paramCaras.Trasera.dNt = dNtValue.dNt;
    dNtValue = this.calc_D(this.paramCaras.Superior.dnt1);
    this.paramCaras.Superior.dNt = dNtValue.dNt;
    dNtValue = this.calc_D(this.paramCaras.Inferior.dnt1);
    this.paramCaras.Inferior.dNt = dNtValue.dNt;
    this.lineChartProDNT = new Chart(this.lineCanvasProDNT.nativeElement, {
      type: "line",
      data: {
        labels: this.f,
        datasets: [
          {
            label: "Diferencia de nivel estandarizada derecha",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,255,128,0.4)",
            borderColor: "rgba(0,255,128,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Derecha.dnt1,
            spanGaps: false,
          },
          {
            label: "Diferencia de nivel estandarizada izquierda",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,255,0,255)",
            borderColor: "rgba(255,255,0,255)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Izquierda.dnt1,
            spanGaps: false,
          },
          {
            label: "Diferencia de nivel estandarizada frontal",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,128,255,0.4)",
            borderColor: "rgba(0,128,255,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Frontal.dnt1,
            spanGaps: false,
          },
          {
            label: "Diferencia de nivel estandarizada trasera",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(255,0,0,0.4)",
            borderColor: "rgba(255,0,0,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Trasera.dnt1,
            spanGaps: false,
          },
          {
            label: "Diferencia de nivel estandarizada superior",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,153,153,0.4)",
            borderColor: "rgba(0,153,153,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Superior.dnt1,
            spanGaps: false,
          },
          {
            label: "Diferencia de nivel estandarizada inferior",
            fill: false,
            lineTension: 0.1,
            backgroundColor: "rgba(0,0,153,0.4)",
            borderColor: "rgba(0,0,153,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.paramCaras.Inferior.dnt1,
            spanGaps: false,
          },
        ],
      },
      options: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Pérdida por transmisión (dB)",
              },
            },
          ],
          xAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: "Frecuencia (Hz)",
              },
            },
          ],
        },
        tooltips: {
          callbacks: {
            label: function (tooltipItem, data) {
              var label = data.datasets[tooltipItem.datasetIndex].label || "";

              if (label) {
                label += ": ";
              }
              label += Math.round(tooltipItem.yLabel * 100) / 100;
              return label;
            },
          },
        },
      },
    });
  }

  resultado_pagina() {
    if (this.tl[0] != null) this.plot();
    else
      this.plotAll(
        this.paramCaras.Derecha.tl,
        this.paramCaras.Izquierda.tl,
        this.paramCaras.Frontal.tl,
        this.paramCaras.Trasera.tl,
        this.paramCaras.Superior.tl,
        this.paramCaras.Inferior.tl
      );
    var chartOptions = {
      scales: {
        xAxes: [
          {
            barPercentage: 1,
            categoryPercentage: 0.6,
            scaleLabel: {
              display: true,
              labelString: "Frecuencia (Hz)",
            },
          },
        ],
        yAxes: [
          {
            id: "y-axis-density",
            scaleLabel: {
              display: true,
              labelString: "Nivel de presión sonora (dB)",
            },
          },
        ],
      },
    };

    let cara_unica;
    this.lpTotal();
    if (this.datosBarras.length > 0) {
      this.datosBarras.push(this.and);
      cara_unica = {
        label: "Spl al interior de la sala",
        data: this.datosBarras,
        backgroundColor: "rgba(0, 99, 132, 0.6)",
        borderWidth: 0,
        yAxisID: "y-axis-density",
      };
    }
    if (this.currentView == this.view.proyecto) {
      cara_unica = {
        label: "Spl al interior de la sala",
        data: [, , , , , , , , , , , , , , , , , , , , , , this.and],
        backgroundColor: "rgba(0, 99, 132, 0.6)",
        borderWidth: 0,
        yAxisID: "y-axis-density",
      };
    }
    let cara_derecha;
    if (this.paramCaras.Derecha.sp.length > 0) {
      const sum = this.paramCaras.Derecha.sp.concat([this.and]);
      cara_derecha = {
        label: "CARA DERECHA",
        data: this.paramCaras.Derecha.sp,
        backgroundColor: "rgba(0,255,128,0.4)",
        borderWidth: 0,
        yAxisID: "y-axis-density",
      };
    }
    let cara_izquierda;
    if (this.paramCaras.Izquierda.sp.length > 0) {
      cara_izquierda = {
        label: "CARA IZQUIERDA",
        data: this.paramCaras.Izquierda.sp,
        backgroundColor: "rgba(255, 255, 0, 255)",
        borderWidth: 0,
        yAxisID: "y-axis-density",
      };
    }
    let cara_frontal;
    if (this.paramCaras.Frontal.sp.length > 0) {
      cara_frontal = {
        label: "CARA FRONTAL",
        data: this.paramCaras.Frontal.sp,
        backgroundColor: "rgba(0,128,255,0.4)",
        borderWidth: 0,
        yAxisID: "y-axis-density",
      };
    }
    let cara_trasera;
    if (this.paramCaras.Trasera.sp.length > 0) {
      cara_trasera = {
        label: "CARA TRASERA",
        data: this.paramCaras.Trasera.sp,
        backgroundColor: "rgba(255,0,0,0.4)",
        borderWidth: 0,
        yAxisID: "y-axis-density",
      };
    }
    let cara_superior;
    if (this.paramCaras.Superior.sp.length > 0) {
      cara_superior = {
        label: "CARA SUPERIOR",
        data: this.paramCaras.Superior.sp,
        backgroundColor: "rgba(0,153,153,0.4)",
        borderWidth: 0,
        yAxisID: "y-axis-density",
      };
    }
    let cara_inferior;
    if (this.paramCaras.Inferior.sp.length > 0) {
      cara_inferior = {
        label: "CARA INFERIOR",
        data: this.paramCaras.Inferior.sp,
        backgroundColor: "rgba(0,0,153,0.4)",
        borderWidth: 0,
        yAxisID: "y-axis-density",
      };
    }

    let datos_caras = [];
    if (cara_unica != undefined) datos_caras.push(cara_unica);
    if (cara_derecha != undefined) datos_caras.push(cara_derecha);
    if (cara_izquierda != undefined) datos_caras.push(cara_izquierda);
    if (cara_frontal != undefined) datos_caras.push(cara_frontal);
    if (cara_trasera != undefined) datos_caras.push(cara_trasera);
    if (cara_superior != undefined) datos_caras.push(cara_superior);
    if (cara_inferior != undefined) datos_caras.push(cara_inferior);

    const datos_barra = {
      labels: this.f2,
      datasets: datos_caras,
    };
    this.barrasChart = new Chart(this.lineCanvasBarras.nativeElement, {
      type: "bar",
      data: datos_barra,
      options: chartOptions,
    });

    this.plot_n();

    this.plot_Dnt();

    this.plot_D();

    if (this.tl[0] != null) this.plot();
    else {
      this.plot_LyR(this.paramCaras.Derecha.tl, this.paramCaras.Izquierda.tl);

      this.plot_FyB(this.paramCaras.Frontal.tl, this.paramCaras.Trasera.tl);

      this.plot_UyD(this.paramCaras.Superior.tl, this.paramCaras.Inferior.tl);

      this.plot_LyR_Stc(
        this.paramCaras.Derecha.tl,
        this.paramCaras.Izquierda.tl
      );

      this.plot_FyB_Stc(this.paramCaras.Frontal.tl, this.paramCaras.Trasera.tl);

      this.plot_UyD_Stc(
        this.paramCaras.Superior.tl,
        this.paramCaras.Inferior.tl
      );

      this.plot_Dproyecto();

      this.plot_DNTproyecto();
    }
  }

  validaInput(e) {
    let event = e || window.event;

    let charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  print() {
    var mywindow = window.open("", "PRINT", "height=400,width=600");

    mywindow.document.write("<html><head><title>Impresión</title>");
    mywindow.document.write("</head><body style=\"width='100%'\">");
    mywindow.document.write(
      "<div style='width: 100%'><h3>INFORME DE RESULTADOS PÉRDIDA POR TRANSMISIÓN</h3></div>"
    );
    if (this.barrasChart != undefined) {
      mywindow.document.write(
        "<br><br><br><br><div style='width: 100%margin-top: 5rem;'><h3>DIAGRAMA DE BARRAS NIVEL DE PRESIÓN EQUIVALENTE</h3></div>"
      );
      mywindow.document.write(
        '<img src="' +
          this.barrasChart.toBase64Image() +
          '" style="width: 100%;"/>'
      );
    }

    if (this.currentView == this.view.proyecto) {
      if (this.lineChart != undefined) {
        mywindow.document.write(
          "<br><br><br><br><br><br><div style='width: 100%margin-top: 5rem; '><h3>GRAFICA PÉRDIDA POR TRANSMISIÓN</h3></div>"
        );
        mywindow.document.write(
          '<img src="' +
            this.lineChart.toBase64Image() +
            '"  style="width: 100%;"/>'
        );
      }
      if (this.lineChartNc != undefined) {
        mywindow.document.write(
          "<br><br><br><br><br><br><br><br><br><br><br><br><br><br> <div style='width: 100%margin-top: 7rem; '><h3>CURVAS NC </h3></div>"
        );
        mywindow.document.write(
          '<img src="' +
            this.lineChartNc.toBase64Image() +
            '"  style="width: 100%;"/>'
        );
      }
      if (this.lineChartRL != undefined) {
        mywindow.document.write(
          "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><div style='width: 100%margin-top: 7rem;'><h3>INDICADORES R'w CARAS DERECHA E IZQUIERDA</h3></div>"
        );
        mywindow.document.write(
          '<img src="' +
            this.lineChartRL.toBase64Image() +
            '"  style="width: 100%;"/>'
        );
      }
      if (this.lineChartFB != undefined) {
        mywindow.document.write(
          "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><div style='width: 100%margin-top: 7rem;'><h3>INDICADORES R'w CARAS FRONTAL Y TRASERA</h3></div>"
        );
        mywindow.document.write(
          '<img src="' +
            this.lineChartFB.toBase64Image() +
            '"  style="width: 100%;"/>'
        );
      }
      if (this.lineChartUP != undefined) {
        mywindow.document.write(
          "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><div style='width: 100%margin-top: 7rem;'><h3>INDICADORES R'w CARAS SUPERIOR E INFERIOR </h3></div>"
        );
        mywindow.document.write(
          '<img src="' +
            this.lineChartUP.toBase64Image() +
            '"  style="width: 100%;"/>'
        );
      }
      if (this.lineChartRLstc != undefined) {
        mywindow.document.write(
          "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><div style='width: 100%margin-top: 7rem; '><h3>MAGNITUDES STC PARA CARAS DERECHA E IZQUIERDA</h3></div>"
        );
        mywindow.document.write(
          '<img src="' +
            this.lineChartRLstc.toBase64Image() +
            '"  style="width: 100%;"/>'
        );
      }
      if (this.lineChartFBstc != undefined) {
        mywindow.document.write(
          "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><div style='width: 100%margin-top: 7rem; '><h3>MAGNITUDES STC PARA CARAS FRONTAL Y TRASERA </h3></div>"
        );
        mywindow.document.write(
          '<img src="' +
            this.lineChartFBstc.toBase64Image() +
            '"  style="width: 100%;"/>'
        );
      }
      if (this.lineChartUPstc != undefined) {
        mywindow.document.write(
          "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><div style='width: 100%margin-top: 7rem;'><h3>MAGNITUDES STC PARA CARAS SUPERIOR E INFERIOR</h3></div>"
        );
        mywindow.document.write(
          '<img src="' +
            this.lineChartUPstc.toBase64Image() +
            '"  style="width: 100%;"/>'
        );
      }

      if (this.lineChartProDNT != undefined) {
        mywindow.document.write(
          "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><div style='width: 100%margin-top: 7rem;'><h3>MAGNITUDES GLOBALES DNT PROYECTO</h3></div>"
        );
        mywindow.document.write(
          '<img src="' +
            this.lineChartProDNT.toBase64Image() +
            '"  style="width: 100%;"/>'
        );
      }
      if (this.lineChartProD != undefined) {
        mywindow.document.write(
          "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><div style='width: 100%margin-top: 7rem;'><h3>MAGNITUDES GLOBALES D PROYECTO</h3></div>"
        );
        mywindow.document.write(
          '<img src="' +
            this.lineChartProD.toBase64Image() +
            '"  style="width: 100%;"/>'
        );
      }
      mywindow.document.write("</body></html>");
      mywindow.document.close(); // necessary for IE >= 10
      mywindow.focus(); // necessary for IE >= 10*/
      setTimeout(() => {
        mywindow.print();
        mywindow.close();
      }, 1000);
    } else {
      if (this.lineChartDnt != undefined) {
        mywindow.document.write(
          "<div style='width: 100%'><h3>MAGNITUDES GLOBALES DNT </h3></div>"
        );
        mywindow.document.write(
          '<img src="' +
            this.lineChartDnt.toBase64Image() +
            '"  style="width: 100%;"/>'
        );
      }

      if (this.lineChartD != undefined) {
        mywindow.document.write(
          "<br><br><br><br><div style='width: 100%;margin-top: 5rem;'><h3>MAGNITUDES GLOBALES D</h3></div>"
        );
        mywindow.document.write(
          '<img src="' +
            this.lineChartD.toBase64Image() +
            '"  style="width: 100%;"/>'
        );
      }

      if (this.lineChart != undefined) {
        mywindow.document.write(
          "<div style='width: 100%'><h3>GRAFICA PÉRDIDA POR TRANSMISIÓN</h3></div>"
        );
        mywindow.document.write(
          '<img src="' +
            this.lineChart.toBase64Image() +
            '"  style="width: 100%;"/>'
        );
      }
      if (this.lineChartNc != undefined) {
        mywindow.document.write(
          "<br><br><br><br><div style='width: 100%;margin-top: 5rem'><h3>CURVAS NC </h3></div>"
        );
        mywindow.document.write(
          '<img src="' +
            this.lineChartNc.toBase64Image() +
            '"  style="width: 100%;"/>'
        );
      }
      mywindow.document.write("</body></html>");
      mywindow.document.close(); // necessary for IE >= 10
      mywindow.focus(); // necessary for IE >= 10*/
      setTimeout(() => {
        mywindow.print();
        mywindow.close();
      }, 1000);
    }
  }

  resetPagina() {
    window.location.reload();
  }
}
