import { Component, OnInit  } from '@angular/core';
import { 
    ModalController, 
    NavParams 
    } from '@ionic/angular';

@Component({
  selector: 'app-modal-condiciones',
  templateUrl: './modalCondiciones.component.html',
})
export class ModalCondiciones implements OnInit {

    data:any = {
        densidad : 0,
        propagacion: 0,
        metodo: ""
    };


    constructor(private modalController: ModalController,
        private navParams: NavParams) {
        }

    ngOnInit() {
        this.data.densidad = this.navParams.data.densidad;
        this.data.propagacion = this.navParams.data.propagacion;
        this.data.metodo = this.navParams.data.metodo;

    }

    setPropagacion(value) {
        this.data.propagacion = value;
    }

    setDensidad(value) {
        this.data.densidad = value;
    }

    setMetodo(value) {
        this.data.metodo = value;
    }


    async closeModal() {

        if (this.data.densidad && this.data.propagacion) {
            
        await this.modalController.dismiss(this.data);

        }

        else{ alert('Ingrese todos los datos');

      }
    }
    dismiss() {

    }



}

