import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InstruccionesPage } from './instrucciones.page';

describe('InstruccionesPage', () => {
  let component: InstruccionesPage;
  let fixture: ComponentFixture<InstruccionesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstruccionesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InstruccionesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
